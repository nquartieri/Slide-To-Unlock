package com.hamondigital.unlock;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * UnlockBar Custom Component.
 * Use the {@link OnUnlockListener} to handle the unlock action.
 */
public class UnlockBar extends RelativeLayout {
	private static final long ANIMATION_DURATION = 300;
	private OnUnlockListener listener = null;

	private TextView text_label = null;
	private ImageView img_thumb = null;

	private int thumbWidth = 0;
	boolean sliding = false;
	private int sliderPosition = 0;
	int initialSliderPosition = 0;
	float initialSlidingX = 0;

	/**
	 * Constructor
	 * @param context The {@link Context}.
     */
	public UnlockBar(Context context) {
		super(context);
		init(context, null);
	}

	/**
	 * Constructor
	 * @param context The {@link Context}.
	 * @param attrs The {@link AttributeSet}.
	 */
	public UnlockBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context, attrs);
	}

	/**
	 * Constructor
	 * @param context The {@link Context}.
	 * @param attrs The {@link AttributeSet}.
	 * @param defStyle The defined style.
	 */
	public UnlockBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context, attrs);
	}

	/**
	 * Sets the {@link UnlockBar} callback.
	 * @param listener The {@link OnUnlockListener}, could be null.
     */
	public void setOnUnlockListener(OnUnlockListener listener) {
		this.listener = listener;
	}

	/**
	 * Init
	 * @param context The {@link Context}.
	 * @param attrs The {@link AttributeSet}.
     */
	private void init(Context context, AttributeSet attrs) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.unlock_bar, this, true);

		// Retrieve layout elements
		text_label = (TextView) findViewById(R.id.text_label);
		img_thumb = (ImageView) findViewById(R.id.img_thumb);

		// Get padding
		thumbWidth = dpToPx(80); // 60dp + 2*10dp
	}

	/**
	 * Reset the  {@link ImageView}
	 */
	public void reset() {
		sliderPosition = 0;
		final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) img_thumb.getLayoutParams();
		ValueAnimator animator = ValueAnimator.ofInt(params.leftMargin, 0);
		animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
		    @Override
		    public void onAnimationUpdate(ValueAnimator valueAnimator) {
		        params.leftMargin = (Integer) valueAnimator.getAnimatedValue();
		        img_thumb.requestLayout();
		    }
		});
		animator.setDuration(ANIMATION_DURATION);
		animator.start();
		text_label.setAlpha(1f);
	}

	@Override
	@SuppressLint("ClickableViewAccessibility")
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);

		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			if (event.getX() > sliderPosition
					&& event.getX() < (sliderPosition + thumbWidth)) {
				sliding = true;
				initialSlidingX = event.getX();
				initialSliderPosition = sliderPosition;
			}
		} else if (event.getAction() == MotionEvent.ACTION_UP
				|| event.getAction() == MotionEvent.ACTION_OUTSIDE) {
			if (sliderPosition >= ((getMeasuredWidth() / 2) - thumbWidth)) {
				if (listener != null) {
					listener.onUnlock();
				}
				// Translate the Slider to the right corner.
				TranslateAnimation animation = new TranslateAnimation(0.0f,
						getMeasuredWidth() - thumbWidth, 0.0f, 0.0f);
				animation.setDuration(ANIMATION_DURATION);
				animation.setFillAfter(true);
				img_thumb.startAnimation(animation);
				text_label.setAlpha(0f);
			} else {
				// Go back with the image to the left corner.
				sliding = false;
				reset();
			}
		} else if (event.getAction() == MotionEvent.ACTION_MOVE && sliding) {
			sliderPosition = (int) (initialSliderPosition
					+ (event.getX() - initialSlidingX));
			if (sliderPosition <= 0) {
				sliderPosition = 0;
			}
			if (sliderPosition >= (getMeasuredWidth() - thumbWidth)) {
				sliderPosition = getMeasuredWidth()  - thumbWidth;
			} else {
				// Reduce alpha according to progress to the right corner.
				int max = getMeasuredWidth() - thumbWidth;
				int progress = (int) (sliderPosition * 100 / (max * 1.0f));
				text_label.setAlpha(1f - progress * 0.02f);
			}
			setMarginLeft(sliderPosition);
		}

		return true;
	}

	/**
	 * Apply the desire margin to the {@link ImageView} to be displace  for unlock.
	 * @param margin The margin.
     */
	private void setMarginLeft(int margin) {
		if (img_thumb == null) return;
		RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) img_thumb.getLayoutParams();
		params.setMargins(margin, 0, 0, 0);
		img_thumb.setLayoutParams(params);
	}

	/**
	 * Convert DP to PX
	 * @param dp The number of DP to convert.
	 * @return The pixels equivalent.
     */
	private int dpToPx(int dp) {
		float density = getResources().getDisplayMetrics().density;
	    return Math.round((float)dp * density);
	}

	/**
	 * UnLock interface. This interface must be implement in post of the observer action.
	 */
	public static interface OnUnlockListener {
		/** Callback action after unlock has being perform */
		void onUnlock();
	}
}
